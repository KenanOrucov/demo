//package com.example.demo.mocito;
//
//import com.example.demo.entity.User;
//import com.example.demo.repository.UserRepository;
//import com.example.demo.service.UserServiceTesting;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.Captor;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.util.List;
//import java.util.Optional;
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//public class UserSpec {
//    @Mock
//    private UserRepository userRepository;
//
//    @Captor
//    private ArgumentCaptor<Long> argumentCaptor;
//
//    UserServiceTesting userServiceTesting;
//
//    @BeforeEach
//    void setUp(){
//        userServiceTesting = new UserServiceTesting(userRepository);
//    }
//
//    @Test
//    public void saveTesterOne(){
//        assertThat(userServiceTesting.saveUser());
//
//        User user = new User(2L, "Kanan", "kanan@mail.com");
//
//        verify(userRepository,times(1)).save(user);
//
//    }
//
//    @Test
//    public void findByIdOne(){
//        when(userRepository.findById(1L)).thenReturn(Optional.of(new User(1L, "Kanan", "kanan")));
//
//        User user = userServiceTesting.findByIdUser(1L);
//
//        verify(userRepository, times(1)).findById(argumentCaptor.capture());
//
//        System.out.println(argumentCaptor.getValue());
//
//        assertThat(user).isEqualTo(new User(1L, "Kanan", "kanan"));
//    }
//
//    @Test
//    public void FindAllUsers(){
//        //arrange  (deyirik ki, 'userRepository.findAll()' - cagirildiqda teze user artir )
//        when(userRepository.findAll()).thenReturn(List.of(new User(1L, "Kanan", "kanan")));
//
//        //act
//        List<User> users = userServiceTesting.findAllUsers();
//
//        //assert
//        assertThat(users).isEqualTo(List.of(new User(1L, "Kananas", "kanan@mail.com")));
//    }
//
//    @Test
//    void updateUser(){
//        User user = new User(1L, "Kanan2", "kanan2@mail.com");
//
//        when(userRepository.save(user)).thenReturn(user);
//
//        User result = userServiceTesting.updateUser(user);
//
//        verify(userRepository, times(1)).save(user);
//
//        assertThat(result).isEqualTo(user);
//    }
//
//    @Test
//    void deleteUserById(){
//        User user = new User(1L, "Kanan", "kanan@mail.com");
//        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
//
//        boolean result = userServiceTesting.deleteUser(1L);
//
//        verify(userRepository, times(1)).findById(user.getId());
//        verify(userRepository, times(1)).delete(user);
//        assertThat(result).isEqualTo(Boolean.TRUE);
//    }
//
//    @Test
//    public void testDeleteUser_UserNotFound() {
//        when(userRepository.findById(12L)).thenReturn(Optional.empty());
//
//        boolean result = userServiceTesting.deleteUser(12L);
//
//        verify(userRepository, never()).delete(any());
//        assertThat(result).isFalse();
//    }
//
//    @Test
//    public void testDeleteUser_UserNotFoundException() {
////        when(userRepository.findById(11L)).thenReturn(Optional.empty());
//
//        Exception exception = assertThrows(Exception.class, () -> {
//            userServiceTesting.deleteUser(11L);
//        });
//
//        assertThat(exception.getMessage()).isEqualTo("User with ID 1 not found");
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
