//package com.example.demo;
//
//import com.example.demo.service.TicTacToe;
//import org.junit.jupiter.api.Test;
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
//
//public class TicTacToeSpec {
//
//    TicTacToe ticTacToe = new TicTacToe();
//
//    @Test
//    void whenXOutSideBoardThenRuntimeException(){
//        assertThatThrownBy(() -> ticTacToe.play(5,2)).isInstanceOf(new RuntimeException("X is the outside of the board").getClass());
//    }
//
//    @Test
//    void whenYOutSideBoardThenRuntimeException(){
//        assertThatThrownBy(() -> ticTacToe.play(2,5)).isInstanceOf(new RuntimeException("Y is the outside of the board").getClass());
//    }
//
//    @Test
//    public void whenOccupiedThenRuntimeException() {
//        ticTacToe.play(2, 1);
//        assertThatThrownBy(() -> ticTacToe.play(2,1)).isInstanceOf(new RuntimeException("Box is occupied").getClass());
//    }
//}
