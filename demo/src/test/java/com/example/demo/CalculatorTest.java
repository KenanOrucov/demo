//package com.example.demo;
//
//import org.assertj.core.internal.Numbers;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.ValueSource;
//
//import java.util.List;
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
//import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//public class CalculatorTest {
//
//    Calculator calculator = new Calculator() ;
//
//    @Test
//    void givenAAndBWhenIAddThenC(){//AAA - style
//        //Arrange
//        int a = 6;
//        int b = 6;
//
//        //Act
//        int c = calculator.add(a,b);
//
//
//        //Assert
//        assertThat(c).isEqualTo(a + b);
//    }
//
//    @Test
//    void givenAAndBWhenSubtractThenC(){//AAA - style
//        //Arrange
//        int a = 6;
//        int b = 6;
//
//        //Act
//        int c = calculator.subtract(a,b);
//
//
//        //Assert
//        assertThat(c).isEqualTo(a - b);
//    }
//
//    @Test
//    void givenAAndBWhenDivideThenC(){//AAA - style
//        //Arrange
//        int a = 10;
//        int b = 2;
//
//        //Act
//        int c = calculator.divide(a,b);
//
//
//        //Assert
//        assertThat(c).isEqualTo(a / b);
//    }
//
//    @Test
//    void givenAAndBWhenDivideByZeroThenException(){//AAA - style
//        //Arrange
//        int a = 10;
//        int b = 0;
//
//        //Assert
//        assertThatThrownBy(() -> calculator.divide(a,b))
//                .isInstanceOf(RuntimeException.class);
//    }
//
//
//    @ParameterizedTest
//    @ValueSource(ints = {1,3,5,-7,15, Integer.MAX_VALUE})
//    void isOddNumbers(int number){
//        assertTrue(isOdd(number));
//    }
//
//    boolean isOdd(int number){
//        return number % 2 != 0;
//    }
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
