package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceTesting {

    public final UserRepository userRepository;

    public UserServiceTesting(UserRepository userRepository) {
        this.userRepository = userRepository;
        User user1 = new User(1L, "Kanan", "kanan@mail.com");
        userRepository.save(user1);

        User user2 = new User(2L, "Baki", "baki@mail.com");
        userRepository.save(user2);

        User user3 = new User(3L, "Sabah", "sabah@mail.com");
        userRepository.save(user3);
    }

    public User saveUser(){
        User user = new User(2L, "Kanan", "kanan@mail.com");
        userRepository.save(user);
        return user;
    }

    public User findByIdUser(Long id){
        return userRepository.findById(id).get();
    }

    public List<User> findAllUsers(){
        List<User> all = userRepository.findAll();
        return all.stream().map(item -> {
            item.setName(item.getName() + "as");
            item.setMail(item.getMail() + "@mail.com");
            return item;
        }).collect(Collectors.toList());
    }

    public User updateUser(User user){
        User user2 = userRepository.save(user);
        return user2;
    }

    public boolean deleteUser(Long id){
        Optional<User> userOptional = userRepository.findById(id);

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            userRepository.delete(user);
            return true;
        }
        if (id != 1){
            try {
                throw new Exception("User with ID 1 not found");
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        }

        return false;
    }
}


















