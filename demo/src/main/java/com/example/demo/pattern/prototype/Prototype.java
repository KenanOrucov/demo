package com.example.demo.pattern.prototype;

public interface Prototype {
    Prototype getCLone();
}
