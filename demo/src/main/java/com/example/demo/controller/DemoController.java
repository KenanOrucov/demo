package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.service.UserServiceTesting;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
@CrossOrigin(exposedHeaders = "Access-Control-Allow-Origin")
@RequiredArgsConstructor
public class DemoController {

    private final UserService userService;

    @GetMapping("/all")
    public List<User> findAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User findId(@PathVariable Long id){
        return userService.getById(id);
    }

    @PostMapping("/add")
    public User addUser(@RequestBody User user){
        return userService.save(user);
    }

    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable Long id){
        return userService.delete(id);
    }
}
