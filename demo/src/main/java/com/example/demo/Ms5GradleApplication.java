package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms5GradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms5GradleApplication.class, args);
	}

}
