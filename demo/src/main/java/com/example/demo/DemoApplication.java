package com.example.demo;

import com.example.demo.pattern.Singleton;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

//		Singleton singleton1 = Singleton.getInstance();
//		Singleton singleton2 = Singleton.getInstance();
//		Singleton singleton3 = Singleton.getInstance();
//		Singleton singleton4 = Singleton.getInstance();
//
//		System.out.println(singleton1);
//		System.out.println(singleton2);
//		System.out.println(singleton3);
//		System.out.println(singleton4);

	}

}
